package tc.translatr.pedidos;

import java.io.*;

import tc.translatr.API.*;

import org.json.simple.JSONObject;
import org.junit.Assert;
import org.junit.Test;

import javax.servlet.http.*;

import static org.mockito.Mockito.*;

public class HelloAppEngineTest{
  @Test
  public void test() throws IOException {
	  
	HttpServletResponse response = mock(HttpServletResponse.class);
	HttpServletRequest request = mock(HttpServletRequest.class);
	
	when(request.getParameter("input")).thenReturn("Firewall");
	
	StringWriter stringWriter = new StringWriter();
    PrintWriter writer = new PrintWriter(stringWriter);
    when(response.getWriter()).thenReturn(writer);
	
    /*TranslatRAPI Translat = mock(TranslatRAPI.class);
    
    when(Translat.askAPI(eq(TranslatRAPI.API.INGLES_APLICADO),any(String.class))).thenReturn(new JSONObject());
    */
	
    new TranslatRAPI().doGet(request,response);
    
    Assert.assertTrue(stringWriter.toString().contains("\"status\":\"OK\""));

    System.out.println(stringWriter.toString());
    

  }
}
