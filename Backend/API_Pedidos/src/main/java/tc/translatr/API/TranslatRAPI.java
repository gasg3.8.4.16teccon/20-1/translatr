package tc.translatr.API;

import tc.translatr.Objects.*;
import tc.translatr.Seguridad.JWTokenHelper;
import tc.translatr.Seguridad.ValidToken;
import tc.translatr.helper.*;
import tc.translatr.helper.Type;

import java.io.*;
import java.util.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.regex.Pattern;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.json.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.MalformedJwtException;

@WebServlet(
	    name = "API-TranslatR",
	    urlPatterns = {"/translatR/API"}
	)

public class TranslatRAPI extends HttpServlet {
	
	private static final String API_PEDIDOS_URL = "https://ahidalgogulasalle.appspot.com/pedidos";
	private static final String GOOGLE_TRANSLATE_URL ="https://translation.googleapis.com/language/translate/v2";
	private static final String LINGUEE_URL="https://linguee-api.herokuapp.com/api";
	
	private JSONObject JSONResponse = new JSONObject();
	private JSONArray JSONResults = new JSONArray();
	private JWTokenHelper jwth=JWTokenHelper.getInstance();
	private ValidToken vToken=ValidToken.getInstance();
	private boolean TokenVal = true;
	
	
	@Override
	 public void doGet(HttpServletRequest request, HttpServletResponse response) 
		      throws IOException {
		
		String pkp=request.getParameter("privateKey");
		
		if(pkp!=null) {
			try {
	             jwth.claimKey(pkp);
	        } catch(Exception e) {
	            if (e instanceof ExpiredJwtException) {
	        		//System.out.println("expirado");
	        		JSONResponse=helper.Error(Type.Parameter.EXPIREDTOKEN);
	        		TokenVal = false;
	            } else if (e instanceof MalformedJwtException) {
	        		//System.out.println("MAL");
	        		JSONResponse=helper.Error(Type.Parameter.MALFORMEDTOKEN);
	        		TokenVal = false;
	            }else  {
	        		//System.out.println("TOKEN");
	        		JSONResponse=helper.Error(Type.Parameter.MALFORMEDTOKEN);
	        		TokenVal = false;
	            }
	        }
		}
		//System.out.println(TokenVal);
		//vToken.getI();
	
		if(vToken.getTokenLLamada(pkp)==false) {
    		//System.out.println("expirado");
    		JSONResponse=helper.Error(Type.Parameter.EXPIREDTOKEN);
    		TokenVal = false;
		}
		if(TokenVal == true) {
			
			vToken.LlamadaRealizada(pkp);
			
			response.addHeader("Access-Control-Allow-Origin", "*");
			response.addHeader("Content-Type", "application/json; charset=UTF-8");
			
			String input = "";
			int[] SourcesCount= {0,0,0};
			JSONResults = new JSONArray();
			JSONResponse = new JSONObject();
			
			if(request.getParameterMap().containsKey("input") && request.getParameter("input")!= "") {
				input=request.getParameter("input");

				if(input.length()!=0) {
					input = helper.Validate(Type.Format.WHITESPACES, request.getParameter("input"));

					
					if(Pattern.matches("[a-zA-Z]+", input)==true) {
						
						if(request.getParameterMap().containsKey("sources")) {
					    	
					    	Pattern pattern = Pattern.compile(",");
					    	String[] Sources = pattern.split(request.getParameter("sources"));
					    	
					    	for(String source:Sources) {
					    	
					    		if(source.compareTo("InglesAplicado")==0) {
					    			if(SourcesCount[0]==0) {
					    			JSONResults.put(askAPI(API.INGLES_APLICADO,helper.Validate(Type.Format.INGLES_APLICADO, input)));
					    			JSONResponse.put("status", "OK");
					    			JSONResponse.put("API", "TranslatR");
								    SourcesCount[0]=1;}
					    		}
					    		else if(source.compareTo("GoogleTranslate")==0) {
					    			if(SourcesCount[1]==0) {
					    			JSONResults.put(askAPI(API.GOOGLE_TRANSLATE,input));
					    			JSONResponse.put("status", "OK");
					    			JSONResponse.put("API", "TranslatR");
								    SourcesCount[1]=1;}
					    		}
					    		else if(source.compareTo("Linguee")==0) {
					    			if(SourcesCount[2]==0) {
					    			JSONResults.put(askAPI(API.LINGUEE,input));
					    			JSONResponse.put("status", "OK");
					    			JSONResponse.put("API", "TranslatR");
								    SourcesCount[2]=1;}
					    		}
					    		else {
					    			JSONResults = new JSONArray();
					    			JSONResponse=helper.Error(Type.Parameter.REQUIRED);
					    			break;
					    		}
					    		
					    	}
					    	
					    }
					    else {
					    	JSONResults.put(askAPI(API.LINGUEE,input));
					    	JSONResults.put(askAPI(API.INGLES_APLICADO,helper.Validate(Type.Format.INGLES_APLICADO, input)));
					    	JSONResults.put(askAPI(API.GOOGLE_TRANSLATE,input));
					    }
						
						System.out.println(JSONResults);//Debugging 28/07
						
						JSONResponse.put("API", "translatR");
						JSONResponse.put("consultedWORD",input);
						JSONResponse.put("results",JSONResults);
					}
					else {
			    		System.out.println("Parameter");
						//response.getWriter().printf(helper.Error(Type.Parameter.REQUIRED));
			    		JSONResponse=helper.Error(Type.Parameter.INVALIDPARAMS);	
					}
					
				}    
				else {

	    			System.out.println("Parameter");
					//response.getWriter().printf(helper.Error(Type.Parameter.REQUIRED));
	    			JSONResponse=helper.Error(Type.Parameter.INVALIDPARAMS);
					
				}
				    
				    
			}
			else {

    			System.out.println("Null Parameter");
				//response.getWriter().printf(helper.Error(Type.Parameter.REQUIRED));
    			JSONResponse=helper.Error(Type.Parameter.REQUIRED);
				
			}
			
		}
			TokenVal = true;
			response.getWriter().print(JSONResponse.toString());
	}

	public static enum API{
		INGLES_APLICADO,
		GOOGLE_TRANSLATE,
		LINGUEE
	}
	
	@SuppressWarnings("unchecked")
	public JSONObject askAPI(API api,String word) {
		
		URL url = null;

		try {
			switch (api) {
			
			case INGLES_APLICADO:{
				url = new URL(API_PEDIDOS_URL+"?word="+word);
				break;}
			case GOOGLE_TRANSLATE:{
				
				String source= "source=en";
				String target= "target=es";

				String key="key=AIzaSyAKpdVrkPg08vEYbm9g1sA_Htw_tdLNduQ";
				String params= "&"+source+"&"+target+"&"+key ; 	
				
				url = new URL(GOOGLE_TRANSLATE_URL+"?q="+word+params);
				break;}
			case LINGUEE:{
				String source = "src=en";
				String target = "dst=es";
				
				String params = "&"+source+"&"+target;
				
				url = new URL(LINGUEE_URL+"?q="+word+params);
				break;}
			}
			
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("GET");
			
			InputStream inputStreamResponse = connection.getInputStream();
			JSONParser jsonParser = new JSONParser();
			
			JSONObject response = new JSONObject(convertInputStreamToString(inputStreamResponse));
			
			JSONObject result = new JSONObject();
			
			switch (api) {
			case INGLES_APLICADO:{
				result = new JSONObject(new Result("Ingles Aplicado",response.getJSONArray("results").getJSONObject(0).getString("meaning")).toJSONString());
				break;}
			case GOOGLE_TRANSLATE:{
				result = new JSONObject(new Result("Google Translate",response.getJSONObject("data").getJSONArray("translations").getJSONObject(0).getString("translatedText")).toJSONString());
				break;}
			case LINGUEE:{
				
				if(response.has("exact_matches")) {
				
					JSONArray translations = ((JSONArray) ((JSONObject) ((JSONArray) response.get("exact_matches")).get(0)).get("translations"));
					Example examples[] = {};
						
					JSONObject JSONtranslation = (JSONObject) translations.get(0);
						
					if(!((JSONArray) JSONtranslation.get("examples")).isEmpty()) {
								
							JSONObject JSONExample = (JSONObject) ((JSONArray) JSONtranslation.get("examples")).get(0);
							
							ArrayList<Example> examplesList = new ArrayList<Example>(Arrays.asList(examples));
							
							examplesList.add(new Example("en",JSONExample.getString("source")));
							examplesList.add(new Example("es",JSONExample.getString("target")));	
							
							examples = examplesList.toArray(examples);
								
					}
					
					if(JSONtranslation.getJSONObject("word_type").has("pos")&&!JSONtranslation.getJSONObject("word_type").has("gender")) {
						result = new JSONObject(new Result("Linguee",JSONtranslation.getString("text"),new WordType (JSONtranslation.getJSONObject("word_type").getString("pos")),examples).toJSONString());
					}
					
					else {
						result = new JSONObject(new Result("Linguee",JSONtranslation.getString("text"),new WordType (JSONtranslation.getJSONObject("word_type").getString("pos"),JSONtranslation.getJSONObject("word_type").getString("gender")),examples).toJSONString());
					}	
					
					break;}
			}
			}
			
			return result;
			
		}catch(IOException e){
			System.out.println(e);
			return new JSONObject();
		}
	}

	private static String convertInputStreamToString(InputStream inputStream)
			throws IOException {

		        ByteArrayOutputStream result = new ByteArrayOutputStream();
		        byte[] buffer = new byte[1024];
		        int length;
		        while ((length = inputStream.read(buffer)) != -1) {
		            result.write(buffer, 0, length);
		        }

		        return result.toString();

		    }
	
}
