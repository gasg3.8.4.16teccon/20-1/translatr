package tc.translatr.Objects;

public class Result {

	String consultedAPI;
	String translatedWORD;
	WordType wordtype;
	Example examples[];
	
	public Result(String consultedAPI, String translatedWORD) {
		this.consultedAPI = consultedAPI;
		this.translatedWORD = translatedWORD;
		this.wordtype = null;
		this.examples = null;
	}
	
	
	public Result(String consultedAPI, String translatedWORD, WordType wordtype) {
		this.consultedAPI = consultedAPI;
		this.translatedWORD = translatedWORD;
		this.wordtype = wordtype;
		this.examples = null;
	}
	
	public Result(String consultedAPI, String translatedWORD, WordType wordtype, Example examples[]) {
		this.consultedAPI = consultedAPI;
		this.translatedWORD = translatedWORD;
		this.wordtype = wordtype;
		this.examples = examples;
	}

	public String toJSONString() {
		
		String returnString ="{";
		returnString += "\"consultedAPI\":\""+consultedAPI+"\",";
		returnString += "\"translatedWORD\":\""+translatedWORD+"\"";
		if(wordtype!=null) {
			returnString += ",\"wordTYPE\":"+wordtype.toJSONString();
		}
		if(examples!=null) {
			String examplesString = ",\"examples\":[";
			for(int m=0;m<examples.length;m++) {
				examplesString += examples[m].toJSONString()+",";
			}
			examplesString.substring(0, examplesString.length()-1);
			examplesString += "]";
			returnString += examplesString;
		}
		returnString += "}";
		return returnString;
		
	}
	
}
