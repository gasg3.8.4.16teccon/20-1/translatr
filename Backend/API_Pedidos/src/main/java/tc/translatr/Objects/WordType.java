package tc.translatr.Objects;

public class WordType {

	String pos;
	String gender;
	String number;
	
	@Override
	public String toString() {
		return "WordType ["+ pos + ", " + gender + ", " + number + "]";
	}

	public WordType() {
		this.pos = null;
		this.gender = null;
		this.number = null;
	}
	
	public WordType(String pos) {
		this.pos = pos;
		this.gender = null;
		this.number = null;
	}
	
	public WordType(String pos, String gender) {
		this.pos = pos;
		this.gender = gender;
		this.number = null;
	}
	
	public WordType(String pos, String gender, String number) {
		this.pos = pos;
		this.gender = gender;
		this.number = number;
	}

	public String toJSONString() {
		
		String returnString = "{\"pos\":\""+pos+"\"";
		
		if(gender!=null) {
			returnString += ",\"gender\":\""+gender+"\"";
		}
		if(number!=null) {
			returnString += ",\"number\":\""+number+"\"";
		}
		
		returnString += "}";
		return returnString;
		
	}
	
}
