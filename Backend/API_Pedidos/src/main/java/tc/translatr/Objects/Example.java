package tc.translatr.Objects;

public class Example {

	String language;
	String example;
	
	public Example(String language, String example) {
		this.language = language;
		this.example = example;
	}
	
	@Override
	public String toString() {
		return "Example [language=" + language + ", example=" + example + "]";
	}

	public String toJSONString() {
		return "{\"language\":\""+language+"\",\"example\":\""+example+"\"}";
	}
	
}
