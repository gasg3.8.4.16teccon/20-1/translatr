package tc.translatr.pedidos;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;


@WebServlet(
    name = "API-Pedidos",
    urlPatterns = {"/pedidos"}
)

public class Pedidos extends HttpServlet {

	private static final String GET_WORDS_URL = "http://inglesaplicado.pe.hu/lumen/public/api/word";
	private static final String GET_MEANINGS_URL = "http://inglesaplicado.pe.hu/lumen/public/api/meaning";
	
	private int wordIndex = -1;
	
	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) 
	      throws IOException {
	
		String word = " ";
		wordIndex=-1;
		
		if(request.getParameterMap().containsKey("word")) {
			
			word = request.getParameter("word");
		}

	    response.getWriter().print(askPedidos(word).toString());
	    
	    
	  }
	
	private JSONObject askPedidos(String word) {
		
		JSONObject JSONResponse = new JSONObject();
		JSONResponse.put("word",word);
		
		try {
			
			getIndex(word);
			
			JSONArray results = new JSONArray();
			results.add(getMeaning());
			
			JSONResponse.put("results", results);
			
		} catch (IOException | ParseException e) {


			JSONResponse.put("results", "AN EXCEPTION HAS OCURRED");
			
		}
		
		return JSONResponse;
		
	}
	
	private JSONArray getWords() throws IOException, ParseException{
		
		URL url = new URL(GET_WORDS_URL);
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		connection.setRequestMethod("GET");
		
		InputStream inputStreamResponse = connection.getInputStream();
		JSONParser jsonParser = new JSONParser();
		
		JSONArray response = (JSONArray) jsonParser.parse(new InputStreamReader(inputStreamResponse, "UTF-8"));
		
		return response;
		
	}

	private void getIndex(String searchedWord) throws IOException, ParseException {
		
		JSONArray Words = getWords();

		Words.forEach(word -> {
			
			JSONObject jsonWord = (JSONObject) word; 
			
			if(jsonWord.get("word_text").toString().compareTo(searchedWord)==0) {
				wordIndex += Integer.parseInt(jsonWord.get("id").toString());
			}
			
		});
		
	};
	
	private JSONObject getMeaning() throws IOException, ParseException {
		
		URL url = new URL(GET_MEANINGS_URL);
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		connection.setRequestMethod("GET");
		
		InputStream inputStreamResponse = connection.getInputStream();
		JSONParser jsonParser = new JSONParser();
		
		JSONArray response = (JSONArray) jsonParser.parse(new InputStreamReader(inputStreamResponse, "UTF-8"));
		
		if(wordIndex!=-1) {
			JSONObject wordEntry = (JSONObject) response.get(wordIndex);
			JSONObject wordMeaning = new JSONObject();
			wordMeaning.put("meaning", wordEntry.get("meaning_text").toString());
			return wordMeaning;
			}
		else {
			JSONObject wordMeaning = new JSONObject();
			wordMeaning.put("meaning", "No meaning was found for this word.");
			return wordMeaning;
		}
	}

}
