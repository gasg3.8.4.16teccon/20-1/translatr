package tc.translatr.helper;

import org.json.JSONObject;
import org.json.JSONArray;

public class helper {
	
	private static JSONObject JSONErrorResponse(String status,String message) {
		
		JSONObject errorResponse = new JSONObject();
		
		errorResponse.put("status", status);
		errorResponse.put("message", message);
		errorResponse.put("results", new JSONArray());
		
		return errorResponse;
		
	}
	
	public static JSONObject Error(Type.Parameter err) {
		
		switch(err) {
		case INVALID:
			return JSONErrorResponse("INVALID_REQUEST","Has Ingresado una fuente inválida");
		case INVALIDPARAMS:
			return JSONErrorResponse("INVALID_PARAMS","Has Ingresado una fuente inválida");
		case EXPIREDTOKEN:
			return JSONErrorResponse("EXPIRED_TOKEN","El token ingresado ha expirado");
		case MALFORMEDTOKEN:
			return JSONErrorResponse("INVALID_TOKEN","Token incorrecto");
		case REQUIRED:
			return JSONErrorResponse("INVALID_REQUEST","No has ingresado los Parametros Requeridos");
		default:
			return JSONErrorResponse("WTF","It's literally impossible you got here, go away");
		}
		
	}

	public static String Validate(Type.Format api,String input) {

		switch(api) {
		case WHITESPACES:
			input=input.trim();
			if(input.contains(" ")) {
				return input.substring(0,input.indexOf(' '));}
			else
				return input;
		case INGLES_APLICADO:
			return input.substring(0,1).toUpperCase() + input.substring(1);
		default:
			return "";
		}
		
	}
	
}
