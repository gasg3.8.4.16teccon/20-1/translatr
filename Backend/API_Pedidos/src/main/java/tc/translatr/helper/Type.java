package tc.translatr.helper;

public class Type {

	public enum Parameter{
		INVALID,
		REQUIRED,
		INVALIDPARAMS,
		EXPIREDTOKEN,
		MALFORMEDTOKEN
	}
	
	public enum Format{
		WHITESPACES,
		INGLES_APLICADO,
		GOOGLE_TRANSLATE
	}
	
}
