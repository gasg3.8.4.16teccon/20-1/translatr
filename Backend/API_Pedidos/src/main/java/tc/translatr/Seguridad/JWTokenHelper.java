package tc.translatr.Seguridad;


import java.util.Date;
import java.util.concurrent.TimeUnit;


import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import java.security.Key;

import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.JwtException;

public class JWTokenHelper {

	private static JWTokenHelper jWTokenHelper = null;
	private static final long EXPIRATION_LIMIT = 43800;
	private Key key = Keys.secretKeyFor(SignatureAlgorithm.HS256);
	private boolean stado=false;
	
	public boolean isStado() {
		return stado;
	}
	public void setStado() {
		this.stado = true;
		System.out.println("TRUE");
	}
	
	private JWTokenHelper() {
	}
	public static JWTokenHelper getInstance() {
		if(jWTokenHelper == null)
			jWTokenHelper = new JWTokenHelper();
	    return jWTokenHelper;
	 }

	public String generatePrivateKey(String username, String password) {
		 String jwth=Jwts
	                .builder()
	                .setSubject(username)
	                .setSubject(password)
	                .setExpiration(getExpirationDate())
	                .signWith(key)
	                .compact();
		 
		 return jwth;
	   }
/*
	  public void claimKey(String privateKey) throws ExpiredJwtException, MalformedJwtException  {
	          try {

			    Jwts.parserBuilder().setSigningKey(key).build().parseClaimsJws(privateKey);

			    System.out.println("OK, we can trust this JWT");

			} catch (JwtException e) {

				System.out.println("don't trust the JWT!");
			}
	   }*/
	public void claimKey(String privateKey) throws ExpiredJwtException, MalformedJwtException  {
		 // try {
		    Jwts.parserBuilder().setSigningKey(key).build().parseClaimsJws(privateKey);
		   /* System.out.println("OK, we can trust this JWT");	
		  } catch (JwtException e) {
	
			  System.out.println("don't trust the JW");
			}*/

	  }

	private Date getExpirationDate() {
	       long currentTimeInMillis = System.currentTimeMillis();
	       long expMilliSeconds = TimeUnit.MINUTES.toMillis(EXPIRATION_LIMIT);
	       return new Date(currentTimeInMillis + expMilliSeconds);
	   }

	
}
