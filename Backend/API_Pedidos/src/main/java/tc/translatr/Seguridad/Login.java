package tc.translatr.Seguridad;

import java.io.IOException;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;

@WebServlet(
	    name = "API-Login",
	    urlPatterns = {"/translatR/Auth"}
	)
public class Login extends HttpServlet{
	
	private JSONObject JSONResponse = new JSONObject();
	private JWTokenHelper jwth=JWTokenHelper.getInstance();
	private ValidToken vToken=ValidToken.getInstance();
	
	@Override
	 public void doPost(HttpServletRequest request, HttpServletResponse response) 
		      throws IOException {
		
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Content-Type", "application/json; charset=UTF-8");
		
		String usuario = request.getHeader("user");
		String password = request.getHeader("password");
		JSONResponse.clear();
		String privatekey=JWTokenHelper.getInstance().generatePrivateKey(usuario, password);
		jwth.setStado();
		vToken.add(privatekey);
		JSONResponse.put("privateKey", privatekey);
		response.getWriter().print(JSONResponse.toString());
		
	}
}
