import 'dart:convert';
import 'dart:core';
import 'dart:io';

import 'package:flutter/material.dart';

import 'package:http/http.dart' as http;

Color lightPurple = Color(0xFFDB8FFF);
Color purple = Color(0xFF9854B8);
Color darkPurple = Color(0xFF59316B); 

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        buttonColor: lightPurple,
        accentColor: lightPurple,
        primaryColor: darkPurple,
        scaffoldBackgroundColor: darkPurple,
        visualDensity: VisualDensity.adaptivePlatformDensity,
        textTheme: Theme.of(context).textTheme.apply(
          displayColor: darkPurple,
          bodyColor: darkPurple,
        ),
        dividerTheme: DividerThemeData(
          thickness: 0.5,
          space: 32,
          color: darkPurple,
        )
      ),
      home: MyHomePage(title: 'TranslatR'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  Future <List<dynamic>> translat(String word) async{

    String AUTH_URL = "https://ahidalgogulasalle.appspot.com/translatR/Auth?user=admin&password=admin";
    String API_URL = "https://ahidalgogulasalle.appspot.com/translatR/API?input=$word";

    API_URL += sources;
    var response;

    try{

      //var privateKey = jsonDecode((await http.post(Uri.encodeFull(AUTH_URL))).body)["privateKey"];
      //print(Uri.encodeFull(privateKey)); //Debugging print
      //API_URL += "&privateKey=$privateKey";
      API_URL += "&privateKey=eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE1OTg2ODA1NjB9.hImW7twYNoGmhb9cnoETy4Fjc4WizicfjAjW5E_dQOM";

      print(Uri.encodeFull(API_URL)); //Debugging print

      response = await http.get(Uri.encodeFull(API_URL));

    }catch(e, stacktrace){
      print("This was thrown: $e $stacktrace");
    }
    finally{

      if(response!=null){
        
        Map<String,dynamic> JSONResponse = jsonDecode(response.body);

        if(JSONResponse["status"]=="OK"||!JSONResponse.containsKey("status")){

          List<dynamic> results = JSONResponse["results"];
          print(results);
          return results;

        }

      }

    }

  }

  List results;
  String sources = "";

  bool loading = false;

  void doSources(List<String> scs){

    if (scs.isEmpty && sources.isEmpty)
      return;

    if(scs.isNotEmpty){
      sources = "&sources="+scs.join(",");
    }

  }

  List<Widget> getResults(){
    if(results!=null){
      return results.map((e)=>Results(
        result: e,
        onCanceled: (){
          setState(() {
            results.remove(e);
          });
        },
        )).toList();
    }

    return [];
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Padding(
          padding: EdgeInsets.all(16),
          child: Row(
            children: [
              ConstrainedBox(
                constraints: BoxConstraints(
                  maxWidth: 400,
                  minWidth: 0,
                  maxHeight: double.infinity,
                  minHeight: 0
                ),
                child: Primary(
                  onSearch: (term, sources) async{
                    doSources(sources);
                    setState(() {
                      results = null;
                      loading = true;
                    });
                    var resultados = await translat(term);
                    setState((){
                      results = resultados;
                      loading = false;
                    });
                  },
                )
              ),
              Expanded(
                child: SingleChildScrollView(
                  padding: EdgeInsets.all(16),
                  child: loading?Center(
                    child: CircularProgressIndicator(),
                  ):Column(
                    children: getResults(),
                  ),
                )
              ),
            ],
          )
      )
    );
  }
}

class Primary extends StatefulWidget{
  final Function(String term, List<String> sources) onSearch;
  
  Primary({this.onSearch});

  @override
  PrimaryState createState() => PrimaryState();
}

class PrimaryState extends State<Primary>{

  List<String> sources  = [];
  TextEditingController controller = TextEditingController();
  static const String inglesAplicado = "InglesAplicado", googleTranslate = "GoogleTranslate", linguee = "Linguee";

  update(){
    print(sources);
    setState((){});
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      color: purple,
      elevation: 15,
      borderRadius: BorderRadius.circular(24),
      textStyle: Theme.of(context).textTheme.bodyText2.apply(
        color: Colors.white,
        decorationColor: Colors.white
      ),
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 24, vertical: 16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Align(
              alignment: Alignment.center,
              child: Padding(
                padding: EdgeInsets.symmetric(vertical: 16),
                child: Text("TranslatR", textAlign: TextAlign.center,style: TextStyle(fontSize: 42, fontWeight: FontWeight.bold, color: darkPurple),),
              ),
            ),
            Text("Escribe la pabra que quieres buscar"),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 16),
              child: TextField(
                controller: controller,
                decoration: InputDecoration(
                  filled: true,
                  fillColor: Colors.white,
                  contentPadding: EdgeInsets.symmetric(vertical:8, horizontal:16),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(18),
                    borderSide: BorderSide.none
                  ),
                  hintText: "Ingresa la palabra a consultar"),
              )
            ),
            Align(
              alignment: Alignment.centerRight,
              child:RaisedButton(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15)
                ),
                onPressed: ()=> widget.onSearch(controller.text, sources),
                child: Text("Consultar"),
              )),
            Text("Selecciona las fuentes que deseas consultar:"),
            RowCheck(
              name: "Inglés Aplicado",
              value: inglesAplicado,
              global: sources,
              onChanged: update
            ),
            RowCheck(
              name: "Google Translate",
              value: googleTranslate,
              global: sources,
              onChanged: update
            ),
            RowCheck(
              name: "Linguee",
              value: linguee,
              global: sources,
              onChanged: update
            ),
          ],
        ),
      )
    );
  }
}

class RowCheck extends StatelessWidget{

  final String name, value;
  final List<String> global;
  final void Function() onChanged;

  RowCheck({this.value, this.name, this.global, this.onChanged});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Checkbox(
          value: global.contains(value), 
          onChanged: (v){
            if(v){
              global.add(value);
            }else{
              global.remove(value);
            }

            onChanged.call();
          } 
        ),
        Text(name)
      ]
    );
  }
}

class Results extends StatelessWidget{
  final Map result;
  final Function onCanceled;
  Results({this.result,this.onCanceled});

  List<Widget> optWidgt(Map<dynamic,dynamic> aResult,BuildContext context){

    List<Widget> returnList = [];

    if(aResult.containsKey("wordTYPE")){
      returnList.add(Padding( padding: EdgeInsets.only(bottom: 5) ,child:Text("Categoría Gram./Part of Speech",style: Theme.of(context).textTheme.subtitle1.copyWith(fontWeight: FontWeight.bold))));
      returnList.add(Padding( padding: EdgeInsets.only(left: 80, bottom: 10) ,child:Text("${aResult["wordTYPE"]["pos"]}")));
      if(aResult["wordTYPE"].containsKey("gender")){
        returnList.add(Padding( padding: EdgeInsets.only(left: 80) ,child:Text("Género/Gender",style: Theme.of(context).textTheme.subtitle2.copyWith(fontWeight: FontWeight.bold))));
        returnList.add(Padding( padding: EdgeInsets.only(left: 120, bottom: 10) ,child:Text("${aResult["wordTYPE"]["gender"]}")));
      }
    }
    if(aResult.containsKey("examples")){
      returnList.add(Padding( padding: EdgeInsets.only(bottom: 5) ,child:Text("Ejemplos/Examples",style: Theme.of(context).textTheme.subtitle1.copyWith(fontWeight: FontWeight.bold))));
      List<dynamic> Expls = aResult["examples"];
      print(aResult["examples"]);
      Expls.forEach((exp) { 
        print(exp["language"]);//debugging print
        print(exp["language"].toString());//debugging print
        if(exp["language"].toString()=="en"){
          returnList.add(Padding( padding: EdgeInsets.only(left: 80) ,child:Text("Inglés/English",style: Theme.of(context).textTheme.subtitle2.copyWith(fontWeight: FontWeight.bold))));
        }
        if(exp["language"].toString()=="es"){
          returnList.add(Padding( padding: EdgeInsets.only(left: 80) ,child:Text("Español/Spanish",style: Theme.of(context).textTheme.subtitle2.copyWith(fontWeight: FontWeight.bold))));
        }
        returnList.add(Padding( padding: EdgeInsets.only(left: 120, bottom: 10) ,child:Text("${exp["example"]}")));

      });
    }
    return returnList;

  }

  @override
  Widget build(BuildContext context) {
    if (result.isNotEmpty)
      return Padding(
      padding: EdgeInsets.only(top: 16),
      child: Container(
      padding: EdgeInsets.all(25),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(25),
      ),
      width: 500,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[

          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Text("${result["consultedAPI"]}", textAlign: TextAlign.end ,style: Theme.of(context).textTheme.headline5.copyWith(fontWeight: FontWeight.bold)),
              IconButton(icon: Icon(Icons.close), onPressed: onCanceled )
            ],
          ),
          Divider(),
          Padding( padding: EdgeInsets.only(bottom: 5) ,child:Text("Traducción/Transalation",style: Theme.of(context).textTheme.subtitle1.copyWith(fontWeight: FontWeight.bold))),
          Padding( padding: EdgeInsets.only(left: 80, bottom: 10) ,child:Text("${result["translatedWORD"]}"))]
          +optWidgt(result,context)
      )
    ));
    else
      return SizedBox();
  }
}
