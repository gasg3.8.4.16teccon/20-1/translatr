# TranslatR

TranslatR es una aplicación que provee un diccionario en línea, mostrando no sólo la traducción, sino también los sinónimos y antónimos de una palabra. Consultando de varias fuentes.

Todos los servicios web de TransltR se despliegan en Google AppEngine, y son codificados en Java, en front-end sin embargo, es desarrollado en flutter y desplegado con Firebase Hosting.

## Utiliza Nuestra Web-App

Puedes encontrar nuestra web-app en https://ahidalgogulasalle.web.app/ 

## Utiliza Nuestra API

Utiliza nuestra API haciendo un pedido `GET` a la siguente dirección:

`https://ahidalgogulasalle.appspot.com/translatR/API?parametros`

**Parametros Requeridos**

`input` — La palabra o cadena de texto con la que desees utilizar nuestros servicios de traducción.

**Parametros Opcionales**

`sources` — Las fuentes que desees incluir al hacer la consulta. Esribelos separados por una coma, asegúrate de escribir fuentes válidas, consulta nuestras fuentes disponibles en el apartado [Fuentes Disponibles](#fuentes-disponibles). Por defecto se utilizaran todas las fuentes por lo que si deseas excluir una, deberas listar todas las que si quieres incluir en este parámetro. 

### Fuentes Disponibles

- `InglesAplicado`
- `GoogleTranslate`

# Contribuye al Desarrollo

Siéntete libre de clonar o descargar este repositorio y hacer cuantos pull request consideres necesarios. Recuerda **siempre** trabajar en una rama.

Necesitarás:

* [JDK 8](https://www.oracle.com/technetwork/es/java/javase/downloads/index.html) - Para el desarrollo del Back-End
* [Flutter](https://flutter.dev/) - Para el desarrollo del Front-End

Además te recomendamos utilizar las Cloud Tools de Google para el IDE de tu preferencia ([Eclipse](https://cloud.google.com/eclipse/docs/creating-new-webapp), [Visual Studio](https://cloud.google.com/visual-studio)).
